
#include <iostream>
#include <string>
#include "ucase.h"

int main() {
	using namespace ucase;
	using std::string;
	using std::cout;
	using std::endl;
	string s("оМGNU LInux РуссКий ТесТС");
	string q = toLower(s);
	string p = toUpper(s);
	cout<<"source : "<<s<<" "<<s.size()<<endl;
	cout<<"lower : "<<q<<" "<<q.size()<<endl;
	cout<<"upper : "<<p<<" "<<p.size()<<endl;
	return 0;
}