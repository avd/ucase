/*
    Copyright (c) 2013, Alexander Demakin
    All rights reserved.
*/

#include <functional>
#include <vector>
#include <algorithm>
#include <string>
#include "ucase.h"

namespace ucase {
	
rune to(int _case, rune r, CaseRange* caseRange) {
	if (_case < 0 || MaxCase <= _case) {
		return ReplacementChar; // as reasonable an error as any
	}
	// binary search over ranges
	int32_t lo = 0;
	int32_t hi = CASE_RANGES_SIZE;
	while (lo < hi) {
		int32_t m = lo + (hi-lo)/2;
		CaseRange cr = caseRange[m];
		if (static_cast<rune>(cr.Lo) <= r && r <= static_cast<rune>(cr.Hi)) {
			rune delta = static_cast<rune>(cr.Delta.runes[_case]);
			if (delta > MaxRune) {
				// In an Upper-Lower sequence, which always starts with
				// an UpperCase letter, the real deltas always look like:
				//	{0, 1, 0}    UpperCase (Lower is next)
				//	{-1, 0, -1}  LowerCase (Upper, Title are previous)
				// The characters at even offsets from the beginning of the
				// sequence are upper case; the ones at odd offsets are lower.
				// The correct mapping can be done by clearing or setting the low
				// bit in the sequence offset.
				// The constants UpperCase and TitleCase are even while LowerCase
				// is odd so we take the low bit from _case.
				return static_cast<rune>(cr.Lo) + (((r - static_cast<rune>(cr.Lo)) & ~1) |  static_cast<rune>(_case & 1));
			}
			return r + delta;
		}
		if (r < static_cast<rune>(cr.Lo)) {
			hi = m;
		} else {
			lo = m + 1;
		}
	}
	return r;
}

rune runeToLower(rune r) {
	if (r <= MaxASCII) {
		if ('A' <= r && r <= 'Z') {
			r += 'a' - 'A';
		}
		return r;
	}
	return to(LowerCase, r, CaseRanges);
}

rune runeToUpper(rune r) {
	if (r <= MaxASCII) {
		if ('a' <= r && r <= 'z') {
			r -= 'a' - 'A';
		}
		return r;
	}
	return to(UpperCase, r, CaseRanges);
}

// DecodeRune unpacks the first UTF-8 encoding in p and returns the rune and its width in bytes.
// If the encoding is invalid, it returns (RuneError, 1), an impossible result for correct UTF-8.
// An encoding is invalid if it is incorrect UTF-8, encodes a rune that is
// out of range, or is not the shortest possible UTF-8 encoding for the
// value. No other validation is performed.
rune decodeRune(const char* p, int srcLen, int* const size) {
	rune r;
	
	if (srcLen < 1) {
		*size = 0;
		return RuneError;
	}
	
	uint8_t c0 = static_cast<uint8_t>(p[0]);

	// 1-byte, 7-bit sequence?
	if (c0 < tx) {
		*size = 1;
		return static_cast<rune>(c0);
	}

	// unexpected continuation byte?
	if (c0 < t2) {
		*size = 1;
		return RuneError;
	}

	// need first continuation byte
	if (srcLen < 2) {
		*size = 1;
		return RuneError;
	}
	
	uint8_t c1 = static_cast<uint8_t>(p[1]);
	if (c1 < tx || t2 <= c1) {
		*size = 1;
		return RuneError;
	}

	// 2-byte, 11-bit sequence?
	if (c0 < t3) {
		r = static_cast<rune>(c0 & mask2) << 6 | static_cast<rune>(c1 & maskx);
		if (r <= rune1Max) {
			*size = 1;
			return RuneError;
		}
		*size = 2;
		return r;
	}

	// need second continuation byte
	if (srcLen < 3) {
		*size = 1;
		return RuneError;
	}
	
	uint8_t c2 = static_cast<uint8_t>(p[2]);
	if (c2 < tx || t2 <= c2) {
		*size = 1;
		return RuneError;
	}

	// 3-byte, 16-bit sequence?
	if (c0 < t4) {
		r = static_cast<rune>(c0 & mask3)<<12 | static_cast<rune>(c1 & maskx)<<6 | static_cast<rune>(c2 & maskx);
		if (r <= rune2Max) {
			*size = 1;
			return RuneError;
		}
		if (surrogateMin <= r && r <= surrogateMax) {
			*size = 1;
			return RuneError;
		}
		*size = 3;
		return r;
	}

	// need third continuation byte
	if (srcLen < 4) {
		*size = 1;
		return RuneError;
	}
	uint8_t c3 = static_cast<uint8_t>(p[3]);
	if (c3 < tx || t2 <= c3) {
		*size = 1;
		return RuneError;
	}

	// 4-byte, 21-bit sequence?
	if (c0 < t5) {
		r = static_cast<rune>(c0 & mask4)<<18 |
			static_cast<rune>(c1 & maskx)<<12 |
			static_cast<rune>(c2 & maskx)<<6 |
			static_cast<rune>(c3 & maskx);
		if (r <= rune3Max || MaxRune < r) {
			*size = 1;
			return RuneError;
		}
		*size = 4;
		return r;
	}

	// error
	*size = 1;
	return RuneError;
}

// RuneLen returns the number of bytes required to encode the rune.
// It returns -1 if the rune is not a valid value to encode in UTF-8.
int runeLen(rune r) {
	if (r < 0)
		return -1;
	if (r <= rune1Max)
		return 1;
	if (r <= rune2Max)
		return 2;
	if (surrogateMin <= r && r <= surrogateMax)
		return -1;
	if (r <= rune3Max)
		return 3;
	if (r <= MaxRune)
		return 4;
	return -1;
}

int encodeRune(char* p, rune r) {
	// Negative values are erroneous.  Making it unsigned addresses the problem.
	if (static_cast<uint32_t>(r) <= rune1Max) {
		p[0] = static_cast<uint8_t>(r);
		return 1;
	}

	if (static_cast<uint32_t>(r) <= rune2Max) {
		p[0] = t2 | static_cast<uint8_t>(r>>6);
		p[1] = tx | (static_cast<uint8_t>(r) & maskx);
		return 2;
	}

	if (static_cast<uint32_t>(r) > MaxRune) {
		r = RuneError;
	}

	if (surrogateMin <= r && r <= surrogateMax) {
		r = RuneError;
	}

	if (static_cast<uint32_t>(r) <= rune3Max) {
		p[0] = t3 | static_cast<uint8_t>(r>>12);
		p[1] = tx | (static_cast<uint8_t>(r>>6) & maskx);
		p[2] = tx | (static_cast<uint8_t>(r) & maskx);
		return 3;
	}

	p[0] = t4 | static_cast<uint8_t>(r>>18);
	p[1] = tx | (static_cast<uint8_t>(r>>12) & maskx);
	p[2] = tx | (static_cast<uint8_t>(r>>6) & maskx);
	p[3] = tx | (static_cast<uint8_t>(r) & maskx);
	return 4;
}

std::string mapString(std::function<rune(rune)> mapping, const std::string& s) {
	using std::vector;
	// In the worst case, the string can grow when mapped, making
	// things unpleasant.  But it's so rare we barge in assuming it's
	// fine.  It could also shrink but that falls out naturally.
	// The output buffer b is initialized on demand, the first
	// time a character differs.
	vector<char> result;
	const size_t size = s.size();
	const char* src = s.c_str();
	size_t destIdx = 0;
	int srcRuneLen = 0;
	result.reserve(size);
	for (size_t srcIdx = 0; srcIdx < size;) {
		rune r = decodeRune(&src[srcIdx], size - srcIdx, &srcRuneLen);
		rune newRune = mapping(r);
		if (result.size() == 0) {
			if (r == newRune) {
				srcIdx += srcRuneLen;
				destIdx += srcRuneLen;
				continue;
			}
			result.insert(result.begin(), s.begin(), s.begin() + srcIdx);
		}
		if (newRune >= 0) {
			char buff[4];
			if (result.capacity() - result.size() < UTFMax) {
				result.reserve(size * 2 + UTFMax);
			}
			int destRuneLen = encodeRune(buff, newRune);
			result.insert(result.end(), std::begin(buff), std::begin(buff) + destRuneLen);
			destIdx += destRuneLen;
		}
		srcIdx += srcRuneLen;
	}
	if (result.size() == 0) {
		return s;
	}
	result.push_back(0);
	return std::string(&result[0]);
}

std::string toLower(const std::string& s) {return mapString(runeToLower, s);}
std::string toUpper(const std::string& s) {return mapString(runeToUpper, s);}
	
} // namespace ucase