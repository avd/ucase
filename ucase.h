/*
    Copyright (c) 2013, Alexander Demakin
    All rights reserved.
*/

/*
 * This library provides simple case conversions for utf-8 strings.
 * It has been ported from golang source v 1.1 (golang.org)
 * Code from the following files has been used:
 * 		src/pkg/string/strings.go
 * 		src/pkg/unicode/tables.go
 * 		src/pkg/unicode/letter.go
 * 		src/pkg/unicode/utf8/utf.go
 */

#ifndef UCASE_H_
#define UCASE_H_

#include <stdint.h>
#include <string>

namespace ucase {

typedef int32_t rune;

const int  UTFMax	       = 4;

const rune MaxASCII        = 0x007F;       // maximum ASCII value
const rune ReplacementChar = 0xFFFD;       // Represents invalid code points.
const rune MaxRune         = 0x0010FFFF;   // Maximum valid Unicode code point
const rune UpperLower	   = MaxRune + 1 ; // (Cannot be a valid delta.)

// Code points in the surrogate range are not valid for UTF-8.
const rune surrogateMin    = 0xD800;
const rune surrogateMax    = 0xDFFF;

const rune  RuneSelf  = 0x80;
const rune  RuneError = 0xFFFD;
const rune	rune1Max  = (1 << 7) - 1;
const rune	rune2Max  = (1 << 11) - 1;
const rune	rune3Max  = (1 << 16) - 1;

const uint8_t t1 = 0x00; // 0000 0000
const uint8_t tx = 0x80; // 1000 0000
const uint8_t t2 = 0xC0; // 1100 0000
const uint8_t t3 = 0xE0; // 1110 0000
const uint8_t t4 = 0xF0; // 1111 0000
const uint8_t t5 = 0xF8; // 1111 1000

const uint8_t maskx = 0x3F; // 0011 1111
const uint8_t mask2 = 0x1F; // 0001 1111
const uint8_t mask3 = 0x0F; // 0000 1111
const uint8_t mask4 = 0x07; // 0000 0111

enum DeltaIndices { UpperCase, LowerCase, TitleCase, MaxCase };

struct d {
    d(int a, int b, int c) {
        runes[0] = a;
        runes[1] = b;
        runes[2] = c;
    }
    rune runes[MaxCase];
};

struct CaseRange  {
	uint32_t Lo;
	uint32_t Hi;
	d Delta;
};

static CaseRange CaseRanges[] = {
	{0x0041, 0x005A, d{0, 32, 0}},
	{0x0061, 0x007A, d{-32, 0, -32}},
	{0x00B5, 0x00B5, d{743, 0, 743}},
	{0x00C0, 0x00D6, d{0, 32, 0}},
	{0x00D8, 0x00DE, d{0, 32, 0}},
	{0x00E0, 0x00F6, d{-32, 0, -32}},
	{0x00F8, 0x00FE, d{-32, 0, -32}},
	{0x00FF, 0x00FF, d{121, 0, 121}},
	{0x0100, 0x012F, d{UpperLower, UpperLower, UpperLower}},
	{0x0130, 0x0130, d{0, -199, 0}},
	{0x0131, 0x0131, d{-232, 0, -232}},
	{0x0132, 0x0137, d{UpperLower, UpperLower, UpperLower}},
	{0x0139, 0x0148, d{UpperLower, UpperLower, UpperLower}},
	{0x014A, 0x0177, d{UpperLower, UpperLower, UpperLower}},
	{0x0178, 0x0178, d{0, -121, 0}},
	{0x0179, 0x017E, d{UpperLower, UpperLower, UpperLower}},
	{0x017F, 0x017F, d{-300, 0, -300}},
	{0x0180, 0x0180, d{195, 0, 195}},
	{0x0181, 0x0181, d{0, 210, 0}},
	{0x0182, 0x0185, d{UpperLower, UpperLower, UpperLower}},
	{0x0186, 0x0186, d{0, 206, 0}},
	{0x0187, 0x0188, d{UpperLower, UpperLower, UpperLower}},
	{0x0189, 0x018A, d{0, 205, 0}},
	{0x018B, 0x018C, d{UpperLower, UpperLower, UpperLower}},
	{0x018E, 0x018E, d{0, 79, 0}},
	{0x018F, 0x018F, d{0, 202, 0}},
	{0x0190, 0x0190, d{0, 203, 0}},
	{0x0191, 0x0192, d{UpperLower, UpperLower, UpperLower}},
	{0x0193, 0x0193, d{0, 205, 0}},
	{0x0194, 0x0194, d{0, 207, 0}},
	{0x0195, 0x0195, d{97, 0, 97}},
	{0x0196, 0x0196, d{0, 211, 0}},
	{0x0197, 0x0197, d{0, 209, 0}},
	{0x0198, 0x0199, d{UpperLower, UpperLower, UpperLower}},
	{0x019A, 0x019A, d{163, 0, 163}},
	{0x019C, 0x019C, d{0, 211, 0}},
	{0x019D, 0x019D, d{0, 213, 0}},
	{0x019E, 0x019E, d{130, 0, 130}},
	{0x019F, 0x019F, d{0, 214, 0}},
	{0x01A0, 0x01A5, d{UpperLower, UpperLower, UpperLower}},
	{0x01A6, 0x01A6, d{0, 218, 0}},
	{0x01A7, 0x01A8, d{UpperLower, UpperLower, UpperLower}},
	{0x01A9, 0x01A9, d{0, 218, 0}},
	{0x01AC, 0x01AD, d{UpperLower, UpperLower, UpperLower}},
	{0x01AE, 0x01AE, d{0, 218, 0}},
	{0x01AF, 0x01B0, d{UpperLower, UpperLower, UpperLower}},
	{0x01B1, 0x01B2, d{0, 217, 0}},
	{0x01B3, 0x01B6, d{UpperLower, UpperLower, UpperLower}},
	{0x01B7, 0x01B7, d{0, 219, 0}},
	{0x01B8, 0x01B9, d{UpperLower, UpperLower, UpperLower}},
	{0x01BC, 0x01BD, d{UpperLower, UpperLower, UpperLower}},
	{0x01BF, 0x01BF, d{56, 0, 56}},
	{0x01C4, 0x01C4, d{0, 2, 1}},
	{0x01C5, 0x01C5, d{-1, 1, 0}},
	{0x01C6, 0x01C6, d{-2, 0, -1}},
	{0x01C7, 0x01C7, d{0, 2, 1}},
	{0x01C8, 0x01C8, d{-1, 1, 0}},
	{0x01C9, 0x01C9, d{-2, 0, -1}},
	{0x01CA, 0x01CA, d{0, 2, 1}},
	{0x01CB, 0x01CB, d{-1, 1, 0}},
	{0x01CC, 0x01CC, d{-2, 0, -1}},
	{0x01CD, 0x01DC, d{UpperLower, UpperLower, UpperLower}},
	{0x01DD, 0x01DD, d{-79, 0, -79}},
	{0x01DE, 0x01EF, d{UpperLower, UpperLower, UpperLower}},
	{0x01F1, 0x01F1, d{0, 2, 1}},
	{0x01F2, 0x01F2, d{-1, 1, 0}},
	{0x01F3, 0x01F3, d{-2, 0, -1}},
	{0x01F4, 0x01F5, d{UpperLower, UpperLower, UpperLower}},
	{0x01F6, 0x01F6, d{0, -97, 0}},
	{0x01F7, 0x01F7, d{0, -56, 0}},
	{0x01F8, 0x021F, d{UpperLower, UpperLower, UpperLower}},
	{0x0220, 0x0220, d{0, -130, 0}},
	{0x0222, 0x0233, d{UpperLower, UpperLower, UpperLower}},
	{0x023A, 0x023A, d{0, 10795, 0}},
	{0x023B, 0x023C, d{UpperLower, UpperLower, UpperLower}},
	{0x023D, 0x023D, d{0, -163, 0}},
	{0x023E, 0x023E, d{0, 10792, 0}},
	{0x023F, 0x0240, d{10815, 0, 10815}},
	{0x0241, 0x0242, d{UpperLower, UpperLower, UpperLower}},
	{0x0243, 0x0243, d{0, -195, 0}},
	{0x0244, 0x0244, d{0, 69, 0}},
	{0x0245, 0x0245, d{0, 71, 0}},
	{0x0246, 0x024F, d{UpperLower, UpperLower, UpperLower}},
	{0x0250, 0x0250, d{10783, 0, 10783}},
	{0x0251, 0x0251, d{10780, 0, 10780}},
	{0x0252, 0x0252, d{10782, 0, 10782}},
	{0x0253, 0x0253, d{-210, 0, -210}},
	{0x0254, 0x0254, d{-206, 0, -206}},
	{0x0256, 0x0257, d{-205, 0, -205}},
	{0x0259, 0x0259, d{-202, 0, -202}},
	{0x025B, 0x025B, d{-203, 0, -203}},
	{0x0260, 0x0260, d{-205, 0, -205}},
	{0x0263, 0x0263, d{-207, 0, -207}},
	{0x0265, 0x0265, d{42280, 0, 42280}},
	{0x0268, 0x0268, d{-209, 0, -209}},
	{0x0269, 0x0269, d{-211, 0, -211}},
	{0x026B, 0x026B, d{10743, 0, 10743}},
	{0x026F, 0x026F, d{-211, 0, -211}},
	{0x0271, 0x0271, d{10749, 0, 10749}},
	{0x0272, 0x0272, d{-213, 0, -213}},
	{0x0275, 0x0275, d{-214, 0, -214}},
	{0x027D, 0x027D, d{10727, 0, 10727}},
	{0x0280, 0x0280, d{-218, 0, -218}},
	{0x0283, 0x0283, d{-218, 0, -218}},
	{0x0288, 0x0288, d{-218, 0, -218}},
	{0x0289, 0x0289, d{-69, 0, -69}},
	{0x028A, 0x028B, d{-217, 0, -217}},
	{0x028C, 0x028C, d{-71, 0, -71}},
	{0x0292, 0x0292, d{-219, 0, -219}},
	{0x0345, 0x0345, d{84, 0, 84}},
	{0x0370, 0x0373, d{UpperLower, UpperLower, UpperLower}},
	{0x0376, 0x0377, d{UpperLower, UpperLower, UpperLower}},
	{0x037B, 0x037D, d{130, 0, 130}},
	{0x0386, 0x0386, d{0, 38, 0}},
	{0x0388, 0x038A, d{0, 37, 0}},
	{0x038C, 0x038C, d{0, 64, 0}},
	{0x038E, 0x038F, d{0, 63, 0}},
	{0x0391, 0x03A1, d{0, 32, 0}},
	{0x03A3, 0x03AB, d{0, 32, 0}},
	{0x03AC, 0x03AC, d{-38, 0, -38}},
	{0x03AD, 0x03AF, d{-37, 0, -37}},
	{0x03B1, 0x03C1, d{-32, 0, -32}},
	{0x03C2, 0x03C2, d{-31, 0, -31}},
	{0x03C3, 0x03CB, d{-32, 0, -32}},
	{0x03CC, 0x03CC, d{-64, 0, -64}},
	{0x03CD, 0x03CE, d{-63, 0, -63}},
	{0x03CF, 0x03CF, d{0, 8, 0}},
	{0x03D0, 0x03D0, d{-62, 0, -62}},
	{0x03D1, 0x03D1, d{-57, 0, -57}},
	{0x03D5, 0x03D5, d{-47, 0, -47}},
	{0x03D6, 0x03D6, d{-54, 0, -54}},
	{0x03D7, 0x03D7, d{-8, 0, -8}},
	{0x03D8, 0x03EF, d{UpperLower, UpperLower, UpperLower}},
	{0x03F0, 0x03F0, d{-86, 0, -86}},
	{0x03F1, 0x03F1, d{-80, 0, -80}},
	{0x03F2, 0x03F2, d{7, 0, 7}},
	{0x03F4, 0x03F4, d{0, -60, 0}},
	{0x03F5, 0x03F5, d{-96, 0, -96}},
	{0x03F7, 0x03F8, d{UpperLower, UpperLower, UpperLower}},
	{0x03F9, 0x03F9, d{0, -7, 0}},
	{0x03FA, 0x03FB, d{UpperLower, UpperLower, UpperLower}},
	{0x03FD, 0x03FF, d{0, -130, 0}},
	{0x0400, 0x040F, d{0, 80, 0}},
	{0x0410, 0x042F, d{0, 32, 0}},
	{0x0430, 0x044F, d{-32, 0, -32}},
	{0x0450, 0x045F, d{-80, 0, -80}},
	{0x0460, 0x0481, d{UpperLower, UpperLower, UpperLower}},
	{0x048A, 0x04BF, d{UpperLower, UpperLower, UpperLower}},
	{0x04C0, 0x04C0, d{0, 15, 0}},
	{0x04C1, 0x04CE, d{UpperLower, UpperLower, UpperLower}},
	{0x04CF, 0x04CF, d{-15, 0, -15}},
	{0x04D0, 0x0527, d{UpperLower, UpperLower, UpperLower}},
	{0x0531, 0x0556, d{0, 48, 0}},
	{0x0561, 0x0586, d{-48, 0, -48}},
	{0x10A0, 0x10C5, d{0, 7264, 0}},
	{0x1D79, 0x1D79, d{35332, 0, 35332}},
	{0x1D7D, 0x1D7D, d{3814, 0, 3814}},
	{0x1E00, 0x1E95, d{UpperLower, UpperLower, UpperLower}},
	{0x1E9B, 0x1E9B, d{-59, 0, -59}},
	{0x1E9E, 0x1E9E, d{0, -7615, 0}},
	{0x1EA0, 0x1EFF, d{UpperLower, UpperLower, UpperLower}},
	{0x1F00, 0x1F07, d{8, 0, 8}},
	{0x1F08, 0x1F0F, d{0, -8, 0}},
	{0x1F10, 0x1F15, d{8, 0, 8}},
	{0x1F18, 0x1F1D, d{0, -8, 0}},
	{0x1F20, 0x1F27, d{8, 0, 8}},
	{0x1F28, 0x1F2F, d{0, -8, 0}},
	{0x1F30, 0x1F37, d{8, 0, 8}},
	{0x1F38, 0x1F3F, d{0, -8, 0}},
	{0x1F40, 0x1F45, d{8, 0, 8}},
	{0x1F48, 0x1F4D, d{0, -8, 0}},
	{0x1F51, 0x1F51, d{8, 0, 8}},
	{0x1F53, 0x1F53, d{8, 0, 8}},
	{0x1F55, 0x1F55, d{8, 0, 8}},
	{0x1F57, 0x1F57, d{8, 0, 8}},
	{0x1F59, 0x1F59, d{0, -8, 0}},
	{0x1F5B, 0x1F5B, d{0, -8, 0}},
	{0x1F5D, 0x1F5D, d{0, -8, 0}},
	{0x1F5F, 0x1F5F, d{0, -8, 0}},
	{0x1F60, 0x1F67, d{8, 0, 8}},
	{0x1F68, 0x1F6F, d{0, -8, 0}},
	{0x1F70, 0x1F71, d{74, 0, 74}},
	{0x1F72, 0x1F75, d{86, 0, 86}},
	{0x1F76, 0x1F77, d{100, 0, 100}},
	{0x1F78, 0x1F79, d{128, 0, 128}},
	{0x1F7A, 0x1F7B, d{112, 0, 112}},
	{0x1F7C, 0x1F7D, d{126, 0, 126}},
	{0x1F80, 0x1F87, d{8, 0, 8}},
	{0x1F88, 0x1F8F, d{0, -8, 0}},
	{0x1F90, 0x1F97, d{8, 0, 8}},
	{0x1F98, 0x1F9F, d{0, -8, 0}},
	{0x1FA0, 0x1FA7, d{8, 0, 8}},
	{0x1FA8, 0x1FAF, d{0, -8, 0}},
	{0x1FB0, 0x1FB1, d{8, 0, 8}},
	{0x1FB3, 0x1FB3, d{9, 0, 9}},
	{0x1FB8, 0x1FB9, d{0, -8, 0}},
	{0x1FBA, 0x1FBB, d{0, -74, 0}},
	{0x1FBC, 0x1FBC, d{0, -9, 0}},
	{0x1FBE, 0x1FBE, d{-7205, 0, -7205}},
	{0x1FC3, 0x1FC3, d{9, 0, 9}},
	{0x1FC8, 0x1FCB, d{0, -86, 0}},
	{0x1FCC, 0x1FCC, d{0, -9, 0}},
	{0x1FD0, 0x1FD1, d{8, 0, 8}},
	{0x1FD8, 0x1FD9, d{0, -8, 0}},
	{0x1FDA, 0x1FDB, d{0, -100, 0}},
	{0x1FE0, 0x1FE1, d{8, 0, 8}},
	{0x1FE5, 0x1FE5, d{7, 0, 7}},
	{0x1FE8, 0x1FE9, d{0, -8, 0}},
	{0x1FEA, 0x1FEB, d{0, -112, 0}},
	{0x1FEC, 0x1FEC, d{0, -7, 0}},
	{0x1FF3, 0x1FF3, d{9, 0, 9}},
	{0x1FF8, 0x1FF9, d{0, -128, 0}},
	{0x1FFA, 0x1FFB, d{0, -126, 0}},
	{0x1FFC, 0x1FFC, d{0, -9, 0}},
	{0x2126, 0x2126, d{0, -7517, 0}},
	{0x212A, 0x212A, d{0, -8383, 0}},
	{0x212B, 0x212B, d{0, -8262, 0}},
	{0x2132, 0x2132, d{0, 28, 0}},
	{0x214E, 0x214E, d{-28, 0, -28}},
	{0x2160, 0x216F, d{0, 16, 0}},
	{0x2170, 0x217F, d{-16, 0, -16}},
	{0x2183, 0x2184, d{UpperLower, UpperLower, UpperLower}},
	{0x24B6, 0x24CF, d{0, 26, 0}},
	{0x24D0, 0x24E9, d{-26, 0, -26}},
	{0x2C00, 0x2C2E, d{0, 48, 0}},
	{0x2C30, 0x2C5E, d{-48, 0, -48}},
	{0x2C60, 0x2C61, d{UpperLower, UpperLower, UpperLower}},
	{0x2C62, 0x2C62, d{0, -10743, 0}},
	{0x2C63, 0x2C63, d{0, -3814, 0}},
	{0x2C64, 0x2C64, d{0, -10727, 0}},
	{0x2C65, 0x2C65, d{-10795, 0, -10795}},
	{0x2C66, 0x2C66, d{-10792, 0, -10792}},
	{0x2C67, 0x2C6C, d{UpperLower, UpperLower, UpperLower}},
	{0x2C6D, 0x2C6D, d{0, -10780, 0}},
	{0x2C6E, 0x2C6E, d{0, -10749, 0}},
	{0x2C6F, 0x2C6F, d{0, -10783, 0}},
	{0x2C70, 0x2C70, d{0, -10782, 0}},
	{0x2C72, 0x2C73, d{UpperLower, UpperLower, UpperLower}},
	{0x2C75, 0x2C76, d{UpperLower, UpperLower, UpperLower}},
	{0x2C7E, 0x2C7F, d{0, -10815, 0}},
	{0x2C80, 0x2CE3, d{UpperLower, UpperLower, UpperLower}},
	{0x2CEB, 0x2CEE, d{UpperLower, UpperLower, UpperLower}},
	{0x2D00, 0x2D25, d{-7264, 0, -7264}},
	{0xA640, 0xA66D, d{UpperLower, UpperLower, UpperLower}},
	{0xA680, 0xA697, d{UpperLower, UpperLower, UpperLower}},
	{0xA722, 0xA72F, d{UpperLower, UpperLower, UpperLower}},
	{0xA732, 0xA76F, d{UpperLower, UpperLower, UpperLower}},
	{0xA779, 0xA77C, d{UpperLower, UpperLower, UpperLower}},
	{0xA77D, 0xA77D, d{0, -35332, 0}},
	{0xA77E, 0xA787, d{UpperLower, UpperLower, UpperLower}},
	{0xA78B, 0xA78C, d{UpperLower, UpperLower, UpperLower}},
	{0xA78D, 0xA78D, d{0, -42280, 0}},
	{0xA790, 0xA791, d{UpperLower, UpperLower, UpperLower}},
	{0xA7A0, 0xA7A9, d{UpperLower, UpperLower, UpperLower}},
	{0xFF21, 0xFF3A, d{0, 32, 0}},
	{0xFF41, 0xFF5A, d{-32, 0, -32}},
	{0x10400, 0x10427, d{0, 40, 0}},
	{0x10428, 0x1044F, d{-40, 0, -40}}
};

#define CASE_RANGES_SIZE (sizeof(CaseRanges) / sizeof(CaseRanges[0]))

// ToLower returns a copy of the string s with all Unicode letters mapped to their lower case.
std::string toLower(const std::string& s);
// ToUpper returns a copy of the string s with all Unicode letters mapped to their upper case.
std::string toUpper(const std::string& s);

} // namespace ucase

#endif  // UCASE_H_